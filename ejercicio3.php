<!DOCTYPE html>
<html>
<head>
    <title>Verificar tipo de carácter</title>
</head>
<body>
<link rel="stylesheet" href="ejercicio3.css">
    <h1>Verificar tipo de carácter</h1>
    <form method="post" action="">
        Caracter: <input type="text" name="caracter"><br>
        <input type="submit" name="submit" value="Verificar">
    </form>
    <?php
	function verificarCaracter($caracter) {
	    if (ctype_alpha($caracter)) {
	    	if (ctype_upper($caracter)) {
	    		return "Letra mayúscula";
	    	} else {
	    		return "Letra minúscula";
	    	 }
	    } elseif (ctype_digit($caracter)) {
	    	return "Número";
	    } elseif (ctype_punct($caracter)) {
	    	return "Símbolo";
	    } else {
	    	return "No es un carácter válido";
	    }
	}

	if (isset($_POST['submit'])) {
		$caracter = $_POST['caracter'];
		echo "<p>El carácter '$caracter' es: " . verificarCaracter($caracter) . "</p>";
	}
?>
</body>
</html>