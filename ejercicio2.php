<!DOCTYPE html>
<html>
<head>
    <title>Determinar Etapa de Vida</title>
</head>
<body>
<link rel="stylesheet" href="ejercicio2.css">
	<h1>Determinar Etapa de Vida</h1>
	<form method="post" action="">
		<label for="edad">Edad:</label>
		<input type="number" name="edad" id="edad" required><br><br>
		<input type="submit" name="submit" value="Determinar"><br><br>
	</form>

	<?php
		function etapa($edad) {
			if ($edad >= 0 && $edad <= 2) {
				return "Bebé";
			} elseif ($edad >= 3 && $edad <= 5) {
				return "Niño";
			} elseif ($edad >= 6 && $edad <= 12) {
				return "Pubertad";
			} elseif ($edad >= 13 && $edad <= 18) {
				return "Adolescente";
			} elseif ($edad >= 19 && $edad <= 25) {
				return "Joven";
			} elseif ($edad >= 26 && $edad <= 60) {
				return "Adulto";
			} else {
				return "Anciano";
			}
		}

		if (isset($_POST['submit'])) {
			$edad = $_POST['edad'];
			$etapa = etapa($edad);
			echo "<p>La persona se encuentra en la etapa de: " . $etapa . "</p>";
		}
	?>
</body>
</html>