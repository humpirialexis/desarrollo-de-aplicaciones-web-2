<!DOCTYPE html>
<html>
<head>
	<title>Cálculo de área y perímetro de un cuadrado</title>
    
</head>
<body>
	<link rel="stylesheet" href="ejercicios.css">
    <h1>Cálculo de área y perímetro de un cuadrado</h1>
	<form method="post" action="">
		Lado: <input type="number" name="lado"><br><br>
		<input type="submit" name="submit" value="Calcular"><br><br>
	</form>

	<?php
		function cuadrado($lado) {
		    $area = $lado ** 2;
		    $perimetro = $lado * 4;
		    return array($area, $perimetro);
		}

		if (isset($_POST['submit'])) {
			$lado = $_POST['lado'];
			list($area, $perimetro) = cuadrado($lado);
			echo "El área del cuadrado es: " . $area . "<br>";
			echo "El perímetro del cuadrado es: " . $perimetro;
		}
	?>
</body>
</html>